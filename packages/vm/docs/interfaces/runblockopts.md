[ethereumjs-vm](../README.md) > [RunBlockOpts](../interfaces/runblockopts.md)

# Interface: RunBlockOpts

## Hierarchy

**RunBlockOpts**

## Index

### Properties

* [block](runblockopts.md#block)
* [generate](runblockopts.md#generate)
* [root](runblockopts.md#root)
* [skipBlockValidation](runblockopts.md#skipblockvalidation)

---

## Properties

<a id="block"></a>

###  block

**● block**: *`any`*

*Defined in [runBlock.ts:18](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/vm/lib/runBlock.ts#L18)*

___
<a id="generate"></a>

### `<Optional>` generate

**● generate**: *`undefined` \| `false` \| `true`*

*Defined in [runBlock.ts:27](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/vm/lib/runBlock.ts#L27)*

___
<a id="root"></a>

### `<Optional>` root

**● root**: *`Buffer`*

*Defined in [runBlock.ts:22](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/vm/lib/runBlock.ts#L22)*

___
<a id="skipblockvalidation"></a>

### `<Optional>` skipBlockValidation

**● skipBlockValidation**: *`undefined` \| `false` \| `true`*

*Defined in [runBlock.ts:31](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/vm/lib/runBlock.ts#L31)*

___

