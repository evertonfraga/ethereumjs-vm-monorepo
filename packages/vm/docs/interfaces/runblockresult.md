[ethereumjs-vm](../README.md) > [RunBlockResult](../interfaces/runblockresult.md)

# Interface: RunBlockResult

## Hierarchy

**RunBlockResult**

## Index

### Properties

* [receipts](runblockresult.md#receipts)
* [results](runblockresult.md#results)

---

## Properties

<a id="receipts"></a>

###  receipts

**● receipts**: *[TxReceipt](txreceipt.md)[]*

*Defined in [runBlock.ts:41](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/vm/lib/runBlock.ts#L41)*

___
<a id="results"></a>

###  results

**● results**: *[RunTxResult](runtxresult.md)[]*

*Defined in [runBlock.ts:45](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/vm/lib/runBlock.ts#L45)*

___

