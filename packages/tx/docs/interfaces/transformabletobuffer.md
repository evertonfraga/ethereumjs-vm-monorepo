[ethereumjs-tx](../README.md) > [TransformableToBuffer](../interfaces/transformabletobuffer.md)

# Interface: TransformableToBuffer

## Hierarchy

**TransformableToBuffer**

## Index

### Methods

- [toBuffer](transformabletobuffer.md#tobuffer)

---

## Methods

<a id="tobuffer"></a>

### toBuffer

▸ **toBuffer**(): `Buffer`

_Defined in [types.ts:8](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/tx/src/types.ts#L8)_

**Returns:** `Buffer`

---
