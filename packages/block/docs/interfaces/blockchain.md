[ethereumjs-block](../README.md) > [Blockchain](../interfaces/blockchain.md)

# Interface: Blockchain

## Hierarchy

**Blockchain**

## Index

### Methods

- [getBlock](blockchain.md#getblock)

---

## Methods

<a id="getblock"></a>

### getBlock

▸ **getBlock**(hash: _`Buffer`_, callback: _`function`_): `void`

_Defined in [types.ts:75](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/block/src/types.ts#L75)_

**Parameters:**

| Name     | Type       |
| -------- | ---------- |
| hash     | `Buffer`   |
| callback | `function` |

**Returns:** `void`

---
