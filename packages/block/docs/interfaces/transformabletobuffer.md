[ethereumjs-block](../README.md) > [TransformableToBuffer](../interfaces/transformabletobuffer.md)

# Interface: TransformableToBuffer

## Hierarchy

**TransformableToBuffer**

## Index

### Methods

- [toBuffer](transformabletobuffer.md#tobuffer)

---

## Methods

<a id="tobuffer"></a>

### toBuffer

▸ **toBuffer**(): `Buffer`

_Defined in [types.ts:31](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/block/src/types.ts#L31)_

**Returns:** `Buffer`

---
