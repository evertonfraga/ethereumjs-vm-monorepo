[ethereumjs-common](../README.md) > [chainsType](../interfaces/chainstype.md)

# Interface: chainsType

## Hierarchy

**chainsType**

## Indexable

\[key: `string`\]:&nbsp;`any`

## Index

### Properties

- [names](chainstype.md#names)

---

## Properties

<a id="names"></a>

### names

**● names**: _`object`_

_Defined in [types.ts:9](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/common/src/types.ts#L9)_

#### Type declaration

[key: `string`]: `string`

---
