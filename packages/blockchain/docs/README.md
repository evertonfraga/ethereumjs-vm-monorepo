# ethereumjs-blockchain

## Index

### Classes

- [Blockchain](classes/blockchain.md)

### Interfaces

- [BlockchainInterface](interfaces/blockchaininterface.md)
- [BlockchainOptions](interfaces/blockchainoptions.md)

### Type aliases

- [Block](#block)

### Functions

- [callbackify](#callbackify)

---

## Type aliases

<a id="block"></a>

### Block

**Ƭ Block**: _`any`_

_Defined in [index.ts:23](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/blockchain/src/index.ts#L23)_

---

## Functions

<a id="callbackify"></a>

### callbackify

▸ **callbackify**(original: _`any`_): `any`

_Defined in [callbackify.ts:35](https://github.com/ethereumjs/ethereumjs-vm/blob/b6ba20a/packages/blockchain/src/callbackify.ts#L35)_

**Parameters:**

| Name     | Type  |
| -------- | ----- |
| original | `any` |

**Returns:** `any`

---
